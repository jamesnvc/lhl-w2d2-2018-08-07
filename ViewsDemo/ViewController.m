//
//  ViewController.m
//  ViewsDemo
//
//  Created by James Cash on 07-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic,strong) UIView *theView;
@property (nonatomic,strong) NSLayoutConstraint *yConstraint;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = UIColor.cyanColor;
    self.view.tintColor = UIColor.purpleColor;

    CGRect frame = self.view.frame;
    NSLog(@"self = %@ button = %@", NSStringFromCGRect(frame), NSStringFromCGRect(self.theButton.frame));
    CGPoint origin = self.view.frame.origin; // == frame.origin
    CGSize size = self.view.frame.size;
    CGFloat x = self.view.frame.origin.x;
    CGFloat height = self.view.frame.size.height;
    NSLog(@"Origin %@ size %@ x %f h %f", NSStringFromCGPoint(origin), NSStringFromCGSize(size), x, height);

    CGFloat width = CGRectGetWidth(self.view.frame);

    CGFloat midX = self.view.frame.origin.x + self.view.frame.size.width / 2;
    CGFloat midY = CGRectGetMidY(self.view.frame);

    self.theView = [[UIView alloc] initWithFrame:CGRectMake(30, 100, 150, 75)];
    self.theView.backgroundColor = UIColor.magentaColor;
    [self.view addSubview:self.theView];
    self.theView.layer.cornerRadius = 15;

    UIView *otherView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    otherView.backgroundColor = UIColor.yellowColor;
    [self.theView addSubview:otherView];

    // autolayout example

    UIView *autoView = [[UIView alloc] initWithFrame:CGRectZero];
    autoView.translatesAutoresizingMaskIntoConstraints = NO;
    autoView.backgroundColor = UIColor.orangeColor;
    [self.view addSubview:autoView];

    self.yConstraint = [autoView.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor];

    [NSLayoutConstraint
     activateConstraints:
     @[[NSLayoutConstraint
        constraintWithItem:autoView
        attribute:NSLayoutAttributeCenterX
        relatedBy:NSLayoutRelationEqual
        toItem:self.view
        attribute:NSLayoutAttributeCenterX
        multiplier:1.0 constant:0],

       self.yConstraint,

       [autoView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:30],

       [autoView.heightAnchor constraintEqualToConstant:40]
       ]];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"Touched on view controller");
    /*
    CGPoint origin = self.theView.frame.origin;
    origin.y += 50;
    CGRect rect = { origin, self.theView.frame.size };
    self.theView.frame = rect;
    */
    // try changing `frame` below to `bounds`
    NSLog(@"Before animating");
    [UIView animateWithDuration:2 animations:^{
        self.theView.bounds = CGRectOffset(self.theView.bounds,
                                           -10,
                                           0);
        self.theView.backgroundColor = UIColor.blueColor;
        self.yConstraint.constant += 50;
        [self.view layoutIfNeeded];
        NSLog(@"Animating");
    } completion:^(BOOL finished) {
        if (finished) {
            NSLog(@"Finished");
        } else {
            NSLog(@"???");
        }
    }];
    NSLog(@"After animating");

    [self.theButton setTitle:[self.theButton.titleLabel.text stringByAppendingString:@"!"] forState:UIControlStateNormal];

//    self.theView.frame = CGRectInset(self.theView.frame, 10, -30);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
