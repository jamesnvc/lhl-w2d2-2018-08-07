//
//  ViewController.h
//  ViewsDemo
//
//  Created by James Cash on 07-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *theButton;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *greenYConstraint;

@end

